
## Steps

There are two possible approaches when creating a web service:
- Contract-Last: we start with the Java code, and we generate the web service contract (WSDL) from the classes.
- Contract-First: we start with the WSDL contract, from which we generate the Java classes.

Spring-WS only supports the contract-first development style.

### Starting and Creating the project by visiting [https://start.spring.io/](https://start.spring.io/)
- adding the dependence Spring Web Services

### Adding WSDL4J Dependency  to pom.xml
- WSDL4J: allows the creation, representation, and manipulation of WSDL documents. 

### Adding student.xsd file in the resources folder
- We define the format of the request to accept one parameter of the type String(name)
- We define the format of the response, which contains an object of the type student
- We define the student object used in the response.

### Adding JAXB maven plugin to the pom.xml to generate java object from the xsd file
- <schemaDirectory>${project.basedir}/src/main/resources</schemaDirectory> – The location of the XSD file
- <outputDirectory>${project.basedir}/src/main/java</outputDirectory> – Where we want our Java code to be generated to
